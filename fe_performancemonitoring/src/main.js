import Vue from 'vue'
import router from './router'
import App from './App.vue'
import store from './store'
import Toast from "vue-toastification";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret, faHome, faUserTie, faClipboardList, faEquals, faClipboardCheck, faTools, faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

library.add(faUserSecret, faHome, faUserTie, faClipboardList, faEquals, faClipboardCheck, faTools, faTimesCircle)
Vue.component('font-awesome-icon', FontAwesomeIcon)

import "vue-toastification/dist/index.css";
import "./assets/css/style.css";
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(Toast, {
  transition: "Vue-Toastification__bounce",
  maxToasts: 5,
  toastClassName: "glass-toast-class",
  pauseOnHover: false,
  hideProgressBar: true,
});

new Vue({
  router, store,
  render: h => h(App),
}).$mount('#app')

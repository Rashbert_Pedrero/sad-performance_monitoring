<?php

namespace App\Http\Controllers;

use App\Models\Attendance;
use App\Models\Student;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AttendanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt');
    }

    public function index(){
        $student = DB::table('students')
                   ->where('students.section_id', '=', Request()->section_id)
                   ->where('students.instructor_id', '=', Auth::id())
                   ->where('students.semester_id', '=', Request()->semester_id)
                   ->where('students.school_year_id', '=', Request()->school_year_id)
                   ->leftJoin('attendances', function ($join){
                       $join->on('students.id', '=', 'attendances.student_id')
                        ->where('attendances.date', '=', Request()->date);
                   })->select('students.*', 'attendances.status')
                   ->orderBy('students.gender', 'desc')
                   ->orderBy('students.last_name')
                   ->orderBy('students.first_name')->get();
        return response()->json($student);
    }

    public function update(Request $request, $id){

        $attendance = Attendance::where('student_id', $id)->where('semester_id', $request->semester_id)->where('date', $request->date)->first();

        if(empty($attendance)){
            Attendance::create([
                'student_id' => $request->student_id,
                'date' => $request->date,
                'status' => $request->status,
                'semester_id' => $request->semester_id,
                'school_year_id' => $request->school_year_id,
            ]);
        }
        else {
            $attendance->update(['status' => $request->status]);
        }

        return response()->json('Attendance updated successfully!', 200);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OverAllSummaryController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt');
    }

    public function index(){
        $student = Student::with(['examination', 'attendance', 'merit'])->where('instructor_id', Auth::id())
        ->where('section_id', Request()->section_id)
        ->where('semester_id', Request()->semester_id)
        ->where('school_year_id', Request()->school_year_id)
        ->orderBy('students.gender', 'desc')
        ->orderBy('students.last_name', 'asc')
        ->orderBy('students.first_name', 'asc')->get();

        return response()->json($student);
    }
}

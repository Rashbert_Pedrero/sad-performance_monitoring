<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Examination extends Model
{
    use HasFactory;

    protected $fillable = [
        'student_id',
        'examination_type_id',
        'semester_id',
        'school_year_id',
        'score',
        'total'
    ];

    public function examination_type(){
        return $this->belongsTo(ExaminationType::class, 'examination_type_id', 'id');
    }

    public function student(){
        return $this->belongsTo(Student::class, 'student_id', 'id');
    }
}
